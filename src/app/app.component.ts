import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  @ViewChild('f') signupForm: NgForm;
  defaultUserQuestion: string = 'teacher';
  genders: string[] = ['male', 'female'];

  userAnswer: string = '';

  suggestUserName() {
    const suggestedName = 'Superuser';
  }

  // onSubmit(form: NgForm) {
  //   console.log('Form submitted', form);
  // }

  onSubmit() {
    console.log('NgForm using ViewChild', this.signupForm);
  }
}
